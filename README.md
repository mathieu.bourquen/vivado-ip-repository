# ***DO NOT USE FOR PRODUCTION / NE PAS UTILISER EN PRODUCTION***

# HEIA-FR IP repository
This is a repository of Vivado compatible IPs.

## How to use it
Download the entire repository and extract it in a folder.
```bash
cd 
git clone https://gitlab.forge.hefr.ch/mathieu.bourquen/vivado-ip-repository.git
cd vivado-ip-repository
```

Then, in the Vivado project, add the IPs to the project.
 - Click on "Settings"
 - Select IP>Repository
 - Click on the plus button
 - Select the IPs folder
 - Click on "OK"

The IPs are now available in the IP Catalog under the "User repository" tab.
