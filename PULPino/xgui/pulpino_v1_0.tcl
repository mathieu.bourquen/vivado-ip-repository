# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "RISCY_RV32F" -parent ${Page_0}
  ipgui::add_param $IPINST -name "USE_ZERO_RISCY" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ZERO_RV32E" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ZERO_RV32M" -parent ${Page_0}


}

proc update_PARAM_VALUE.RISCY_RV32F { PARAM_VALUE.RISCY_RV32F } {
	# Procedure called to update RISCY_RV32F when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RISCY_RV32F { PARAM_VALUE.RISCY_RV32F } {
	# Procedure called to validate RISCY_RV32F
	return true
}

proc update_PARAM_VALUE.USE_ZERO_RISCY { PARAM_VALUE.USE_ZERO_RISCY } {
	# Procedure called to update USE_ZERO_RISCY when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.USE_ZERO_RISCY { PARAM_VALUE.USE_ZERO_RISCY } {
	# Procedure called to validate USE_ZERO_RISCY
	return true
}

proc update_PARAM_VALUE.ZERO_RV32E { PARAM_VALUE.ZERO_RV32E } {
	# Procedure called to update ZERO_RV32E when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ZERO_RV32E { PARAM_VALUE.ZERO_RV32E } {
	# Procedure called to validate ZERO_RV32E
	return true
}

proc update_PARAM_VALUE.ZERO_RV32M { PARAM_VALUE.ZERO_RV32M } {
	# Procedure called to update ZERO_RV32M when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ZERO_RV32M { PARAM_VALUE.ZERO_RV32M } {
	# Procedure called to validate ZERO_RV32M
	return true
}


proc update_MODELPARAM_VALUE.USE_ZERO_RISCY { MODELPARAM_VALUE.USE_ZERO_RISCY PARAM_VALUE.USE_ZERO_RISCY } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.USE_ZERO_RISCY}] ${MODELPARAM_VALUE.USE_ZERO_RISCY}
}

proc update_MODELPARAM_VALUE.RISCY_RV32F { MODELPARAM_VALUE.RISCY_RV32F PARAM_VALUE.RISCY_RV32F } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RISCY_RV32F}] ${MODELPARAM_VALUE.RISCY_RV32F}
}

proc update_MODELPARAM_VALUE.ZERO_RV32M { MODELPARAM_VALUE.ZERO_RV32M PARAM_VALUE.ZERO_RV32M } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ZERO_RV32M}] ${MODELPARAM_VALUE.ZERO_RV32M}
}

proc update_MODELPARAM_VALUE.ZERO_RV32E { MODELPARAM_VALUE.ZERO_RV32E PARAM_VALUE.ZERO_RV32E } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ZERO_RV32E}] ${MODELPARAM_VALUE.ZERO_RV32E}
}

