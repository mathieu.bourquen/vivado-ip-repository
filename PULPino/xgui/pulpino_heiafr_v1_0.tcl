
# Loading additional proc with user specified bodies to compute parameter values.
source [file join [file dirname [file dirname [info script]]] gui/pulpino_heiafr_v1_0.gtcl]

# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  set USE_ZERO_RISCY [ipgui::add_param $IPINST -name "USE_ZERO_RISCY" -parent ${Page_0} -layout horizontal]
  set_property tooltip {Zero RIscy is a lighter core. Riscy is a faster core.} ${USE_ZERO_RISCY}
  #Adding Group
  set Core_modules [ipgui::add_group $IPINST -name "Core modules" -parent ${Page_0} -layout horizontal]
  set RISCY_RV32F [ipgui::add_param $IPINST -name "RISCY_RV32F" -parent ${Core_modules}]
  set_property tooltip {Enable the floating point unit in the Riscy core} ${RISCY_RV32F}
  set ZERO_RV32E [ipgui::add_param $IPINST -name "ZERO_RV32E" -parent ${Core_modules}]
  set_property tooltip {Use 16 registers for the Zero Riscy instead of 32 registers} ${ZERO_RV32E}
  set ZERO_RV32M [ipgui::add_param $IPINST -name "ZERO_RV32M" -parent ${Core_modules}]
  set_property tooltip {Enable the multiply/divide unit for the Zero Riscy core.} ${ZERO_RV32M}



}

proc update_PARAM_VALUE.RISCY_RV32F { PARAM_VALUE.RISCY_RV32F PARAM_VALUE.USE_ZERO_RISCY } {
	# Procedure called to update RISCY_RV32F when any of the dependent parameters in the arguments change
	
	set RISCY_RV32F ${PARAM_VALUE.RISCY_RV32F}
	set USE_ZERO_RISCY ${PARAM_VALUE.USE_ZERO_RISCY}
	set values(USE_ZERO_RISCY) [get_property value $USE_ZERO_RISCY]
	if { [gen_USERPARAMETER_RISCY_RV32F_ENABLEMENT $values(USE_ZERO_RISCY)] } {
		set_property enabled true $RISCY_RV32F
	} else {
		set_property enabled false $RISCY_RV32F
		set_property value [gen_USERPARAMETER_RISCY_RV32F_VALUE $values(USE_ZERO_RISCY)] $RISCY_RV32F
	}
}

proc validate_PARAM_VALUE.RISCY_RV32F { PARAM_VALUE.RISCY_RV32F } {
	# Procedure called to validate RISCY_RV32F
	return true
}

proc update_PARAM_VALUE.ZERO_RV32E { PARAM_VALUE.ZERO_RV32E PARAM_VALUE.USE_ZERO_RISCY } {
	# Procedure called to update ZERO_RV32E when any of the dependent parameters in the arguments change
	
	set ZERO_RV32E ${PARAM_VALUE.ZERO_RV32E}
	set USE_ZERO_RISCY ${PARAM_VALUE.USE_ZERO_RISCY}
	set values(USE_ZERO_RISCY) [get_property value $USE_ZERO_RISCY]
	if { [gen_USERPARAMETER_ZERO_RV32E_ENABLEMENT $values(USE_ZERO_RISCY)] } {
		set_property enabled true $ZERO_RV32E
	} else {
		set_property enabled false $ZERO_RV32E
		set_property value [gen_USERPARAMETER_ZERO_RV32E_VALUE $values(USE_ZERO_RISCY)] $ZERO_RV32E
	}
}

proc validate_PARAM_VALUE.ZERO_RV32E { PARAM_VALUE.ZERO_RV32E } {
	# Procedure called to validate ZERO_RV32E
	return true
}

proc update_PARAM_VALUE.ZERO_RV32M { PARAM_VALUE.ZERO_RV32M PARAM_VALUE.USE_ZERO_RISCY } {
	# Procedure called to update ZERO_RV32M when any of the dependent parameters in the arguments change
	
	set ZERO_RV32M ${PARAM_VALUE.ZERO_RV32M}
	set USE_ZERO_RISCY ${PARAM_VALUE.USE_ZERO_RISCY}
	set values(USE_ZERO_RISCY) [get_property value $USE_ZERO_RISCY]
	if { [gen_USERPARAMETER_ZERO_RV32M_ENABLEMENT $values(USE_ZERO_RISCY)] } {
		set_property enabled true $ZERO_RV32M
	} else {
		set_property enabled false $ZERO_RV32M
		set_property value [gen_USERPARAMETER_ZERO_RV32M_VALUE $values(USE_ZERO_RISCY)] $ZERO_RV32M
	}
}

proc validate_PARAM_VALUE.ZERO_RV32M { PARAM_VALUE.ZERO_RV32M } {
	# Procedure called to validate ZERO_RV32M
	return true
}

proc update_PARAM_VALUE.USE_ZERO_RISCY { PARAM_VALUE.USE_ZERO_RISCY } {
	# Procedure called to update USE_ZERO_RISCY when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.USE_ZERO_RISCY { PARAM_VALUE.USE_ZERO_RISCY } {
	# Procedure called to validate USE_ZERO_RISCY
	return true
}


proc update_MODELPARAM_VALUE.USE_ZERO_RISCY { MODELPARAM_VALUE.USE_ZERO_RISCY PARAM_VALUE.USE_ZERO_RISCY } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.USE_ZERO_RISCY}] ${MODELPARAM_VALUE.USE_ZERO_RISCY}
}

proc update_MODELPARAM_VALUE.RISCY_RV32F { MODELPARAM_VALUE.RISCY_RV32F PARAM_VALUE.RISCY_RV32F } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RISCY_RV32F}] ${MODELPARAM_VALUE.RISCY_RV32F}
}

proc update_MODELPARAM_VALUE.ZERO_RV32M { MODELPARAM_VALUE.ZERO_RV32M PARAM_VALUE.ZERO_RV32M } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ZERO_RV32M}] ${MODELPARAM_VALUE.ZERO_RV32M}
}

proc update_MODELPARAM_VALUE.ZERO_RV32E { MODELPARAM_VALUE.ZERO_RV32E PARAM_VALUE.ZERO_RV32E } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ZERO_RV32E}] ${MODELPARAM_VALUE.ZERO_RV32E}
}

